package com.balush.model.plateau;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class PlateauTest {

    @BeforeEach
    public void before() {
        System.out.println("before each");
    }

    @Test
    public void testCheckIsValid() {
        Plateau plateau = new Plateau();
        assertFalse(plateau.checkIsValid(0, plateau.getListOfNumbers().size()));
        assertTrue(plateau.checkIsValid(0, plateau.getListOfNumbers().size() - 1));
    }

    @Test
    @Disabled("Not yet")
    public void testIsLongestSequence() {
        Plateau plateau = new Plateau(Arrays.asList(2, 2, 2));
        plateau.calculateLongestPlateau();
        assertFalse(plateau.isLongestSequence(Arrays.asList(1, 1)));
    }

    @ParameterizedTest
    @MethodSource("paramForTest")
    public void testSequenceWithParam(List<Integer> list, int result) {
        Plateau plateau = new Plateau(list);
        plateau.calculateLongestPlateau();
        assertEquals(plateau.getLengthPlateau(), result);
    }

    private static Stream<Arguments> paramForTest() {
        return Stream.of(
                Arguments.of(Arrays.asList(1, 1, 2, 3), 0),
                Arguments.of(Arrays.asList(2, 2, 2, 3), 0),
                Arguments.of(Arrays.asList(1, 3, 3, 3), 3),
                Arguments.of(Arrays.asList(1, 2, 2, 1), 2)
        );
    }

    @Test
    public void testGetSequences() {
        Plateau plateau = new Plateau();
        assertEquals(Arrays.asList(2, 2, 2), plateau.getSequences(2, Arrays.asList(2, 2, 2, 1)));
    }

    @Test
    public void testGetLengthPlateau() {
        Plateau plateau = new Plateau(Arrays.asList(1, 1, 2, 4, 3, 4, 4, 4));
        plateau.calculateLongestPlateau();
        assertEquals(plateau.getLengthPlateau(), 3);
    }

    @Test
    public void testGetLocation() {
        Plateau plateau = new Plateau(Arrays.asList(1, 1, 2, 4, 4, 4, 3, 4));
        plateau.calculateLongestPlateau();
        assertEquals(plateau.getLocation(), 3);
    }
}
