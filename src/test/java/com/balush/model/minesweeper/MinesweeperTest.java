package com.balush.model.minesweeper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MinesweeperTest {

    protected static Logger logger = LogManager.getLogger(MinesweeperTest.class);

    @InjectMocks
    ClearCell clearCell;

    @InjectMocks
    OccupiedCell occupiedCell;

    @Mock
    Cell mockCell;

    @BeforeEach
    public void beforeEach() {
        logger.info("Before each");
    }

    @Test
    public void testGetInstanceClearCell() {
        when(mockCell.getInstance(false)).thenReturn(new ClearCell());

        assertEquals(clearCell.getInstance(false).getClass(), ClearCell.class);
    }

    @Test
    public void testGetInstanceOccupiedCell() {
        when(mockCell.getInstance(true)).thenReturn(new OccupiedCell());

        assertEquals(occupiedCell.getInstance(true).getClass(), OccupiedCell.class);
    }

    @Test
    public void testIncrementNumberOfBombs() {
        Minesweeper minesweeper = new Minesweeper(5, 5, 0.5);
        if (!minesweeper.getField()[1][1].isOccupiedCell()) {
            int i = ((ClearCell) minesweeper.getField()[1][1]).getNumberOfNeighboringBombs() + 1;
            minesweeper.incrementNumberOfBombs(1, 1);
            assertEquals(i, ((ClearCell) minesweeper.getField()[1][1]).getNumberOfNeighboringBombs());
        }
    }

    @Test
    public void testCheckTopLeft() {
        Minesweeper minesweeper = new Minesweeper(5, 5, 0.4);
        System.out.println(minesweeper.getField()[0][0].cell);
        if (!minesweeper.getField()[0][0].cell) {
            assertFalse(minesweeper.checkTopLeft(1, 1));
        } else {
            assertTrue(minesweeper.checkTopLeft(1, 1));
        }
    }

    @Test
    public void testGetStringField() {
        Minesweeper minesweeper = new Minesweeper(3, 3, 0.5);
        assertEquals(minesweeper.getStringField().length(), 22);
    }

}
