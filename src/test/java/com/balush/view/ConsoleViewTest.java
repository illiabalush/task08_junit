package com.balush.view;

import com.balush.controller.Controller;
import com.balush.model.minesweeper.Minesweeper;
import org.junit.Test;
import org.junit.jupiter.api.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConsoleViewTest {

    @InjectMocks
    Controller controller;

    @Mock
    Controller mock;

    @Test
    public void testPrintMines() {
        when(mock.getMinesweeper()).thenReturn(new Minesweeper(3, 3, 0.5));

        assertEquals(mock.getMinesweeper().getStringField().length(), 22);
    }

    @Test
    public void testGetMinesweeper() {
        controller.createMines(5, 5, 1);

        assertNotNull(controller.getMinesweeper());
    }
}
