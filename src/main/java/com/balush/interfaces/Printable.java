package com.balush.interfaces;

public interface Printable {
    void print();
}
