package com.balush.view;

import com.balush.controller.Controller;
import com.balush.interfaces.Printable;

import java.util.*;

public class ConsoleView extends View {
    private Controller controller;
    private Scanner scanner;
    private Map<String, String> menu;
    private Map<String, Printable> menuItems;

    public ConsoleView() {
        controller = new Controller();
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Minesweeper");
        menu.put("2", "2 - Plateau");
        menuItems = new LinkedHashMap<>();
        menuItems.put("1", this::printMines);
        menuItems.put("2", this::printPlateau);
    }

    private void printMines() {
        logger.info("Enter row: ");
        int r = scanner.nextInt();
        logger.info("Enter column: ");
        int c = scanner.nextInt();
        logger.info("Enter probability[0 - 1]: ");
        double p = scanner.nextDouble();
        controller.createMines(r, c, p);
        logger.info(controller.getMinesweeper().getStringField());
        logger.info(controller.getMinesweeper().getStringFieldWithNumber());
    }

    private void printPlateau() {
        List<Integer> list = new ArrayList<>();
        logger.info("Enter your list of numbers: ");
        logger.info("Enter Q for exit!");
        String string = "";
        while (!string.toUpperCase().equals("Q")) {
            logger.info("Enter number: ");
            string = scanner.next();
            if (!string.toUpperCase().equals("Q")) {
                list.add(Integer.valueOf(string));
            }
        }
        controller.createPlateau(list);
        controller.getPlateau().calculateLongestPlateau();
        logger.info(controller.getPlateau().getListOfNumbers());
        logger.info(controller.getPlateau().getLengthPlateau());
        logger.info(controller.getPlateau().getLocation());
    }

    private void showMenu() {
        for (String s : menu.values()) {
            logger.info(s);
        }
    }

    public void start() {
        String keyMenu;
        do {
            showMenu();
            scanner = new Scanner(System.in);
            logger.info("Please, select menu point:");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                menuItems.get(keyMenu).print();
            } catch (Exception ignored) {

            }
        } while (!keyMenu.equals("Q"));
    }

    public Controller getController() {
        return controller;
    }
}
