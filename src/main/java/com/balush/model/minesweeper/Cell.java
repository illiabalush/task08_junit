package com.balush.model.minesweeper;

public abstract class Cell {
    protected boolean cell;

    public boolean isOccupiedCell() {
        return cell;
    }

    public void setCell(boolean cell) {
        this.cell = cell;
    }

    public abstract String getCell();

    public abstract Cell getInstance(boolean p);
}
