package com.balush.model.minesweeper;

public class ClearCell extends Cell {
    private int numberOfNeighboringBombs;

    public ClearCell() {
        cell = false;
        numberOfNeighboringBombs = 0;
    }

    public int getNumberOfNeighboringBombs() {
        return numberOfNeighboringBombs;
    }

    public void setNumberOfNeighboringBombs(int numberOfNeighboringBombs) {
        this.numberOfNeighboringBombs = numberOfNeighboringBombs;
    }

    @Override
    public Cell getInstance(boolean p) {
        return !p ? new ClearCell() : null;
    }

    @Override
    public String getCell() {
        return " ";
    }
}
