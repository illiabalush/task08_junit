package com.balush.model.minesweeper;

import java.util.Random;

public class Minesweeper {
    private Cell[] fieldObjects = {new ClearCell(), new OccupiedCell()};
    private Cell[][] field;
    private int row;
    private int column;
    private double probability;

    public Minesweeper(int r, int c, double p) {
        row = r;
        column = c;
        probability = p;
        calculateField();
        calculateNeighboringBombs();
    }

    private void calculateField() {
        field = new Cell[row][column];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                field[i][j] = getInstanceCell(isOccupied());
            }
        }
    }

    public Cell getInstanceCell(boolean isOccupied) {
        Cell cell;
        for (Cell i : fieldObjects) {
            cell = i.getInstance(isOccupied);
            if (cell != null) {
                return cell;
            }
        }
        return null;
    }

    private boolean isOccupied() {
        int randomNumber = new Random().nextInt(100);
        return randomNumber <= probability * 100;
    }

    private void calculateNeighboringBombs() {
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                if (!field[i][j].isOccupiedCell()) {
                    calculateNumberOfNeighbourBombs(i, j);
                }
            }
        }
    }

    private void calculateNumberOfNeighbourBombs(int positionRow, int positionColumn) {
        if (checkTopLeft(positionRow, positionColumn)) incrementNumberOfBombs(positionRow, positionColumn);
        if (checkTop(positionRow, positionColumn)) incrementNumberOfBombs(positionRow, positionColumn);
        if (checkTopRight(positionRow, positionColumn)) incrementNumberOfBombs(positionRow, positionColumn);
        if (checkLeft(positionRow, positionColumn)) incrementNumberOfBombs(positionRow, positionColumn);
        if (checkRight(positionRow, positionColumn)) incrementNumberOfBombs(positionRow, positionColumn);
        if (checkBotLeft(positionRow, positionColumn)) incrementNumberOfBombs(positionRow, positionColumn);
        if (checkBot(positionRow, positionColumn)) incrementNumberOfBombs(positionRow, positionColumn);
        if (checkBotRight(positionRow, positionColumn)) incrementNumberOfBombs(positionRow, positionColumn);
    }

    public void incrementNumberOfBombs(int r, int c) {
        int numOfBombs = ((ClearCell) field[r][c]).getNumberOfNeighboringBombs();
        ((ClearCell) field[r][c]).setNumberOfNeighboringBombs(numOfBombs + 1);
    }

    public boolean checkTopLeft(int r, int c) {
        return (r - 1 >= 0 && c - 1 >= 0) && (field[r - 1][c - 1].isOccupiedCell());
    }

    private boolean checkTop(int r, int c) {
        return r - 1 >= 0 && field[r - 1][c].isOccupiedCell();
    }

    private boolean checkTopRight(int r, int c) {
        return (r - 1 >= 0 && c + 1 < column) && (field[r - 1][c + 1].isOccupiedCell());
    }

    private boolean checkLeft(int r, int c) {
        return c - 1 >= 0 && field[r][c - 1].isOccupiedCell();
    }

    private boolean checkRight(int r, int c) {
        return c + 1 < column && field[r][c + 1].isOccupiedCell();
    }

    private boolean checkBotLeft(int r, int c) {
        return (r + 1 < row && c - 1 >= 0) && (field[r + 1][c - 1].isOccupiedCell());
    }

    private boolean checkBot(int r, int c) {
        return r + 1 < row && field[r + 1][c].isOccupiedCell();
    }

    private boolean checkBotRight(int r, int c) {
        return (r + 1 < row && c + 1 < column) && (field[r + 1][c + 1].isOccupiedCell());
    }

    public String getStringField() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\n");
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                stringBuilder.append(field[i][j].getCell()).append("|");
            }
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    public String getStringFieldWithNumber() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\n");
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                if (!field[i][j].isOccupiedCell()) {
                    stringBuilder.append(((ClearCell) field[i][j]).getNumberOfNeighboringBombs()).append("|");
                } else {
                    stringBuilder.append(field[i][j].getCell()).append("|");
                }
            }
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public Cell[][] getField() {
        return field;
    }
}
