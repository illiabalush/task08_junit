package com.balush.model.minesweeper;

public class OccupiedCell extends Cell {
    OccupiedCell() {
        cell = true;
    }

    @Override
    public Cell getInstance(boolean p) {
        return p ? new OccupiedCell() : null;
    }

    @Override
    public String getCell() {
        return "*";
    }
}
