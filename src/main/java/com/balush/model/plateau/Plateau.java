package com.balush.model.plateau;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Plateau {
    private List<Integer> listOfNumbers;
    private PlateauParam plateau;

    private class PlateauParam {
        List<Integer> longestPlateau = new ArrayList<>();
        int indexStart;
        int indexEnd;
    }

    public Plateau() {
        listOfNumbers = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            listOfNumbers.add(new Random().nextInt(30) - 15);
        }
        plateau = new PlateauParam();
    }

    public Plateau(List<Integer> list) {
        listOfNumbers = new ArrayList<>(list);
        plateau = new PlateauParam();
    }

    public void calculateLongestPlateau() {
        List<Integer> sequence;
        for (int i = 0; i < listOfNumbers.size() - 1; i++) {
            if ((listOfNumbers.get(i).equals(listOfNumbers.get(i + 1)))) {
                sequence = getSequences(listOfNumbers.get(i), listOfNumbers.subList(i, listOfNumbers.size()));
                if (checkIsValid(i, i + sequence.size() - 1)) {
                    if (isLongestSequence(sequence)) {
                        plateau.longestPlateau = new ArrayList<>(sequence);
                        plateau.indexStart = i;
                        plateau.indexEnd = i + sequence.size();
                    }
                }
                i = i + sequence.size();
            }
        }
    }

    public boolean checkIsValid(int startIndex, int endIndex) {
        if (startIndex - 1 >= 0 && endIndex + 1 < listOfNumbers.size()) {
            return listOfNumbers.get(startIndex) > listOfNumbers.get(startIndex - 1)
                    && listOfNumbers.get(endIndex) > listOfNumbers.get(endIndex + 1);
        } else if (startIndex - 1 < 0 && endIndex + 1 < listOfNumbers.size()) {
            return listOfNumbers.get(endIndex) > listOfNumbers.get(endIndex + 1);
        } else if (startIndex - 1 >= 0 && endIndex + 1 >= listOfNumbers.size()) {
            return listOfNumbers.get(startIndex) > listOfNumbers.get(startIndex - 1);
        } else if (startIndex == 0 && endIndex == listOfNumbers.size() - 1) {
            return true;
        }
        return false;
    }

    public boolean isLongestSequence(List<Integer> sequence) {
        return sequence.size() > plateau.longestPlateau.size();
    }

    public List<Integer> getSequences(int intValue, List<Integer> subList) {
        List<Integer> sequence = new ArrayList<>();
        for (Integer i : subList) {
            if (i == intValue) {
                sequence.add(i);
            } else {
                break;
            }
        }
        return sequence;
    }

    public int getLengthPlateau() {
        return plateau.longestPlateau.size();
    }

    public int getLocation() {
        return plateau.indexStart;
    }

    public List<Integer> getListOfNumbers() {
        return listOfNumbers;
    }
}
