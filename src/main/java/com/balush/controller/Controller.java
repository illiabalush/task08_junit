package com.balush.controller;

import com.balush.model.minesweeper.Minesweeper;
import com.balush.model.plateau.Plateau;

import java.util.List;

public class Controller {
    private Minesweeper minesweeper;
    private Plateau plateau;

    public Controller() {
    }

    public void createMines(int r, int c, double p) {
        minesweeper = new Minesweeper(r, c, p);
    }

    public void createPlateau(List<Integer> list) {
        plateau = new Plateau(list);
    }

    public Minesweeper getMinesweeper() {
        return minesweeper;
    }

    public Plateau getPlateau() {
        return plateau;
    }
}
